# Definition
Let $S:\ C^{2} (\mathbb{R}^{n}) \ni \phi \mapsto S[\phi] \in \mathbb{R}$ be a functional. The variational derivative $\frac{\delta S}{\delta \phi(x)}$ is implicitly defined by acting on some kind of test function $f \in C^{2}(\mathbb{R}^{n})$ in the integral:
$$
\int d^{4}x \frac{\delta S}{\delta \phi(x)} f(x) = \lim_{\varepsilon \to 0} \frac{S[\phi + \varepsilon f] - S[\phi]}{\varepsilon}.
$$
^definition-limit

# Properties
## Simpler Expressions
The variational derivative can also be written as
$$
\int d^{4}x \frac{\delta S}{\delta \phi(x)} f(x) = \left. \frac{d}{d \varepsilon} S[\phi + \varepsilon f] \right|_{\varepsilon = 0}.
$$
^definition-derivative

From this we can expand the functional $S[\phi + \varepsilon f]$ in $\varepsilon$:
$$
S[\phi + \varepsilon f] = S[\phi] + \varepsilon \int d^{4}x \frac{\delta S}{\delta \phi(x)} f(x) + \mathcal{O}(\varepsilon^{2}).
$$
^taylor-expansion

## Linearity
Consider the two functionals $S,F:\ C^{2} (\mathbb{R}^{n}) \ni \phi \mapsto S[\phi], F[\phi] \in \mathbb{R}$ and real numbers $\alpha, \beta \in \mathbb{R}$. From the [[#^definition-limit]] and using the linearity of the „normal“ derivative, we can conclude quite easily that:
$$
\frac{\delta(\alpha S + \beta F)}{\delta \phi (x)} = \alpha \frac{\delta S}{\delta \phi(x)} + \beta \frac{\delta F}{\delta \phi(x)}.
$$

## Product Rule
With the same approach used in [[#Linearity]] we can show that:
$$
\frac{\delta(S \cdot F)}{\delta \phi (x)} = \frac{\delta S}{\delta \phi(x)} F + S \frac{\delta F}{\delta \phi(x)}.
$$

## Chain Rule
The chain rule is somewhat a little bit more difficult to prove, as it can be considered as the continuous generalization of the „conventional“ chain rule:
$$
\begin{aligned}
& \frac{df(y_{\mu}(x))}{dx} = \sum_{\mu} \frac{df(y_\mu(x))}{dy_{\mu}} \frac{dy_{\mu}(x)}{dx} \\
\rightarrow &\frac{\delta S [F[\phi](z)]}{\delta \phi(x)} = \int d^{4}y \frac{\delta S[F[\phi](z)]}{\delta F[\phi](y)} \frac{\delta F[\phi](y)}{\delta \phi(x)},
\end{aligned}
$$
with $S:\ C^{2} (\mathbb{R}^{n}) \ni \phi \mapsto S[\phi] \in \mathbb{R}$ and $F:\ C^{2} (\mathbb{R}^{n}) \ni \phi \mapsto F[\phi](x) \in C^{1} (\mathbb{R})$. This can be proven by applying [[#^definition-derivative]] to the l.h.s. and then applying [[#^taylor-expansion]] once for $S$ & once for $F$, while ignoring $\mathcal{O}(\varepsilon^{2})$.

## Delta Distribution
When considering the specific following functional:
$$
S: C^{2}(\mathbb{R}^{n}) \ni \phi \mapsto S[\phi](y) := \phi(y).
$$
By using [[#^definition-derivative]] with $f(x) = \delta^{(n)}(x-y)$ as a test function, it can be shown that:
$$
\frac{\delta \phi (x)}{\delta \phi (y)} = \delta^{(n)}(x-y).
$$

## Euler-Lagrange Equations
Consider a functional $S$ of the following form:
$$
S[\phi] = \int_{\Omega} d^{4}x\ g(x, \phi(x), \partial_{\mu} \phi(x)),
$$
where $\Omega \subset \mathbb{R}^n$. If we introduce the following constraint:
$$
\left. \frac{\partial g(x, \phi, \partial_{\mu} \phi)}{\partial (\partial_{\mu} \phi)} \right|_{x \in \Omega} = 0,
$$
we obtain the variational derivative by plugging in $S$ into [[#^definition-derivative]] and applying integration by parts using the above constraint to cancel out boundary terms)
$$
\frac{\delta S}{\delta \phi(x)} = \frac{\partial g(x, \phi, \partial_{\mu} \phi)}{\partial \phi} - \sum_{\mu} \partial_{\mu} \frac{\partial g(x, \phi, \partial_{\mu} \phi)}{\partial(\partial_{\mu} \phi)}.
$$
Setting it to $0$ gives us the Euler-Lagrange equations for fields:
$$
\frac{\partial g(x, \phi, \partial_{\mu} \phi)}{\partial \phi} = \sum_{\mu} \partial_{\mu} \frac{\partial g(x, \phi, \partial_{\mu} \phi)}{\partial(\partial_{\mu} \phi)}.
$$