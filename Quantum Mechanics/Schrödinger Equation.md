# In Dirac Notation
The Schrödinger describes the time evolution of a quantum mechanical [[State]] $\ket{\Psi}$, as follow:
$$i \hbar \frac{\partial}{\partial t} \ket{\Psi} = \hat{H} \ket{\Psi},$$
where $\hat{H}$ is the [[Hamilton Operator]]. The solution is given by applying the time evolution operator to the initial state $\ket{\Psi(t_0)}$:
$$\ket{\Psi(t)} = \exp \left( - \frac{i}{\hbar} \hat{H} \cdot (t - t_0) \right) \ket{\Psi{t_0}}$$