# Derivation
In the [[Schrödinger Picture]], [[Schrödinger Equation#In Dirac Notation]] determines the time evolution of a quantum state:
$$\Psi (x, t_{2}) = \bra{x} \exp \left( - \frac{i}{\hbar} \hat{H} \cdot (t_{2} - t_1) \right) \ket{\Psi (t_1)}.$$
Inserting the [[Unit Operator]] $\hat{1} = \int d^{3}x' \ket{x'}\bra{x'}$, we get for the wave functions the following time evolution:
$$
\begin{aligned}
\Psi(x, t_{2}) &= \int d^{3}x' \bra{x} \exp \left( - \frac{i}{\hbar} \hat{H} \cdot (t_{2} - t_1) \right) \ket{x'} \braket{x'|\Psi(t_{1)}} \\
&= \int d^{3}x'\ K(x, x'; t_{2} - t_{1}) \cdot \Psi(x', t_1)
\end{aligned}
$$
where we have defined the propagator as:
$$K(x, x'; t_{2}- t_{1}) := \bra{x} \exp \left( - \frac{i}{\hbar} \hat{H} \cdot (t_{2} - t_1) \right) \ket{x'}.$$

## Equal Times
For equal times $t_{1} = t_{2}$ the propagator yields a 3-dimensional delta function:
$$K(x, x'; t_{1}-t_{1}) = \delta^{3} (x - x')$$

## Infinitesimal Propagator
Let's set $t_{2}-t_{1} = \delta t \rightarrow 0$. Only considering the linear term in $\delta t$ of the time evolution operator, we obtain:
$$
\begin{aligned}
K(x, x'; \delta t) &= \bra{x} \exp \left( - \frac{i}{\hbar} \hat{H} \cdot \delta t \right) \ket{x'} \\
&= \ldots \approx \int d^{3}p \left( \braket{x|p} \braket{p|x'} - \frac{i}{\hbar}\delta t \bra{x}\hat{H}\ket{p}\braket{p|x'} \right)
\end{aligned}
$$
If we assume that $\hat{H} = f_{1}(\hat{p}) + f_{2}({\hat{x}})$, we can write:
$$\bra{x}\hat{H}\ket{p} = H(x,p)\braket{x|p},$$
where $H(x, p)$ is the classical [[Hamilton Function]]. Furthermore, we can use the relation:
$$\braket{x|p} = \left( 2 \pi \hbar \right)^{-1/2} \cdot e^{i \hbar^{-1} \cdot p x},$$
because position and momentum states are related via [[Fourier Transform]]. After a rather lengthy calculation, we find that the infinitesimal propagator in the first order of $\delta t$ is given by:
$$
K(x, x'; \delta t) \approx \frac{1}{N(\delta t)} \exp \left\{ \frac{i \delta t}{\hbar} \left[ \frac{1}{2}m \left( \frac{x - x'}{\delta t} \right)^{2} - V(x) \right] \right\},
$$
^final-form

with the normalization factor:
$$
\frac{1}{N(\delta t)} = \left( \frac{m}{2 \pi i \hbar \delta t} \right)^{2}.
$$
The exponent in [[#^final-form]] already looks **SUS**piciously like the classical [[Lagrangian]]:
$$
L(q, \dot{q}) = \frac{1}{2} m \dot{q}^{2}- V(q).
$$