# Derivation for Quantum Mechanics
Consider the propagator from $(x_{i}, t_{i})$ to $(x_{f}, t_{f})$ (also known as transition amplitude):
$$
K(x_{f}, x_{i}; t_{f} - t_{i}).
$$
Let's say, we measure the particle position at time $t_{1} \in (t_{i}, t_{f})$ to be $x_{1}$, then the following relation has to hold:
$$
K(x_{f}, x_{i}; t_{f}-t_{i}) = K(x_{f}, x_{1}; t_{f}-t_{1}) \cdot K(x_{1}, x_{i}; t_{1} - t_{i}).
$$
If the particle can also be at $x_{1}'$ at time $t_{1}$, we find that:
$$
\begin{aligned}
K(x_{f}, x_{i}; t_{f}-t_{i}) &= K(x_{f}, x_{1}; t_{f}-t_{1}) \cdot K(x_{1}, x_{i}; t_{1} - t_{i}) \\
&+ K(x_{f}, x_{1}'; t_{f}-t_{1}) \cdot K(x_{1}', x_{i}; t_{1} - t_{i})
\end{aligned}
$$
If we now consider, that be particle could be at an arbitrary position $x_{1} \in (-\infty, \infty)$ at time $t_1$, we get that:
$$
K(x_{f}, x_{i}; t_{f}-t_{i}) = \int_{-\infty}^{+\infty} dx_{1}\ K(x_{f}, x_{1}; t_{f}-t_{1}) K(x_{1}, x_{i}; t_{1} - t_{i}).$$
Now let's consider timeseries $t_{i} < t_{1} < t_{2} < \ldots < t_{n} < t_{f}$. We then find that:
$$
\begin{aligned}
K(x_{f}, x_{i}; t_{f}-t_{i}) = \int_{-\infty}^{+\infty} &dx_{1} \ldots dx_{n}\\
&K(x_{f}, x_{n}, t_{f}-t_{n})K(x_{n}, x_{n-1}; t_{n}-t_{n-1})\cdot\ldots \\
&K(x_{1}, x_{i}; t_{1}-t_{i})).
\end{aligned}
$$
For large enough $n$, we can plug in the infinitesimal propagator (not mathematically rigorous) and get:
$$
\begin{aligned}
K(x_{f}, x_{i}; t_{f}-t_{i}) = \frac{1}{N(\delta t)^{n}} \int_{-\infty}^{+\infty} dx_{1} \ldots dx_{n} \exp\left\{ \frac{i \delta t}{\hbar} \left[ \frac{m}{2} \left( \frac{x_{f} - x_{n}}{\delta t} \right)^{2} -V(x_{f}) \right] \right\} \\
\exp\left\{ \frac{i \delta t}{\hbar} \left[ \frac{m}{2} \left( \frac{x_{n} - x_{n-1}}{\delta t} \right)^{2} -V(x_{n}) \right] \right\} \\
\ldots \\
\exp\left\{ \frac{i \delta t}{\hbar} \left[ \frac{m}{2} \left( \frac{x_{1} - x_{i}}{\delta t} \right)^{2} -V(x_{1}) \right] \right\}
\end{aligned}
$$
Let's also call $(x_{n}-x_{n-1}) / \delta t =: (\dot{x})_{n} = \dot{x}(t_{n})$. We can then identify the terms in the exponent as $L(x_{n}, \dot{x}_{n})$:
$$
\begin{aligned}
K(x_{f}, x_{i}; t_{f}-t_{i}) &= \frac{1}{N(\delta t)^{n}} \int_{-\infty}^{+\infty} dx_{1} \ldots dx_{n} \exp\left\{ \frac{i \delta t}{\hbar} L(x_{f}, \dot{x}_{f}) \right\} \cdot \ldots \cdot \exp\left\{ \frac{i \delta t}{\hbar} L(x_{1}, \dot{x}_{1}) \right\} \\
&= \frac{1}{N(\delta t)^{n}} \int_{-\infty}^{+\infty} dx_{1} \ldots dx_{n} \exp\left\{ \frac{i}{\hbar} \left[ L(x_{f}, \dot{x}_{f}) \delta t + \ldots + L(x_{1}, \dot{x}_{1}) \delta t \right] \right\}
\end{aligned}
$$
The r.h.s. of this equation is complex number that depends on $n$. We can take the limit $n \to \infty$, which we define as the path integral:
$$
\begin{aligned}
\lim_{n\to\infty} \frac{1}{N(\delta t)^{n}} \int_{-\infty}^{+\infty} dx_{1} \ldots dx_{n} \exp\left\{ \frac{i}{\hbar} \left[ L(x_{f}, \dot{x}_{f}) \delta t + \ldots + L(x_{1}, \dot{x}_{1}) \delta t \right] \right\} \\
=: \frac{1}{\mathcal{N}} \int \mathcal{D}x\ \exp \left( \frac{i}{\hbar} \int_{t_{i}}^{t_{f}} dt\ L[x(t)] \right) = \frac{1}{\mathcal{N}} \int \mathcal{D}x\ \exp \left( \frac{i}{\hbar} S[x] \right)
\end{aligned}
$$
^qm-version

Strictly speaking, this is *just* another notation for the limit. However, we could interpret $\int \mathcal{D}x$ as an integral over all possible paths and $\exp \left( \frac{i}{\hbar} S[x] \right)$ as the phase that one path $x(t)$ contributes. However, the normalization constant $1/\mathcal{N}$ itself is technically infinite, as well as $\int \mathcal{D}x \ldots$ itself. Thus, one has to be very careful with using the new notation.

# Generalization to Fields
By analogy to [[#^qm-version]], we postulate (in natural units $\hbar = 1$) the path integral for a scalar field $\phi (x) = \phi (t, \underline{x})$ to be the following:
$$
\braket{0, +\infty | 0, -\infty} = \lim_{\substack{t_{1}\to\infty \\ t_{2}\to -\infty}} \braket{0 | \exp\left( i \hat{H} (t_{1}-t_{2}) \right) | 0} = \frac{1}{\mathcal{N}} \int \mathcal{D}\phi(x)\ \exp \left( i S[\phi] \right),
$$
^qft-version

with the action $S$ for a free [[Klein-Gordon Field]] given by:
$$
\begin{aligned}
S[\phi] &= \int_{t_{i}}^{t_{f}} dt\ \int_{\mathbb{R}^{3}} \frac{1}{2} \left\{ (\partial_{\mu} \phi)(\partial^{\mu} \phi) -m^{2} \phi^{2} \right\} \\
&\underbrace{\to}_{\substack{t_{1}\to\infty \\ t_{2}\to -\infty}} \int_{\mathbb{R}^{4}} \frac{1}{2} \left\{ (\partial_{\mu} \phi)(\partial^{\mu} \phi) -m^{2} \phi^{2} \right\}.
\end{aligned}
$$

The term on the r.h.s of [[#^qft-version]] is rather peculiar. It corresponds to the probability of the vacuum state $\ket{0}$ at time $t=-\infty$ to propagate to the vacuum state at $\ket{0}$ at time $t=+\infty$. By definition, the vacuum doesn't change, hence $\braket{0, +\infty | 0, -\infty} =: Z = 1$.