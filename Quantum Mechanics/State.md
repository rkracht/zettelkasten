# Abstract definition
in Quantum Mechanics, elements of a [[Hilbert Space]] $\mathcal{H}$ are called states and denoted in [[Dirac Notation]] as $\ket{\Psi}$

# Wave Function
The wave function of a state $\ket{\Psi}$ in position space is obtained by projecting it onto the eigenfunction of the position operator:
$$\Psi (x, t) = \braket{x|\Psi(t)}$$